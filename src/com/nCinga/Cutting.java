package com.nCinga;

public class Cutting implements Department {
    int productCount=0;

    @Override
    public void increaseCount() {
        productCount++;
    }

    @Override
    public void decreaseCount() {
        productCount--;
        if(productCount<0){
            System.out.println("Product count cannot be negative");
            productCount=0;
        }
    }

    public void printProductCount(){
        System.out.println("Cutting count: "+productCount);
    }


    public  Cutting(int pCount){
        this.productCount=pCount;
    }
    public  Cutting(){

    }
}

package com.nCinga;

public interface Department {
    void increaseCount();
    void decreaseCount();
}

package com.nCinga;

public class Packing implements Department {
    int productCount=0;

    @Override
    public void increaseCount() {
        productCount+=5;
    }

    @Override
    public void decreaseCount() {
        productCount-=5;
        if(productCount<0){
            System.out.println("Product count cannot be negative");
            productCount=0;
        }
    }

    public void printProductCount(){
        System.out.println("Packing count: "+productCount);
    }
    public  Packing(){

    }
    public Packing(int pCount){
        productCount=pCount;
    }

}

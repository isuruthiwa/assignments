package com.nCinga;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Cutting cut1= new Cutting(10);
        cut1.increaseCount();
        cut1.increaseCount();
        cut1.printProductCount();

        cut1.decreaseCount();
        cut1.printProductCount();


        Sewing sew1 = new Sewing();
        sew1.increaseCount();
        sew1.increaseCount();
        sew1.increaseCount();
        sew1.printProductCount();


        Packing pack1 = new Packing();
        for(int i=0; i<10; i++)
            pack1.increaseCount();

        pack1.printProductCount();

        pack1.decreaseCount();

        pack1.printProductCount();
    }
}
